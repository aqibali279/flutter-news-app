import 'dart:async';
import 'package:http/http.dart';
import 'dart:convert';
import 'package:flutter_app/models/auth_cookie.dart';
import 'package:flutter_app/utlis/preferences.dart';
import 'package:flutter_app/utlis/constants.dart';

class UserService {

  static var instance = UserService._instance();
  AuthCookie authCookie;

  UserService._instance();

  Future<AuthCookie> user() async {
    var cookie = await Preferences.instance.getCookie();
    var url = Constants.BASE_URL + 'api/user/get_currentuserinfo/?insecure=cool&cookie=$cookie';
    var result = await get(url);
    print('????????????????????' + result.body + '????????????????????????????????????????????');
    var jsonData = json.decode(result.body);
    authCookie = AuthCookie.fromJson(jsonData);

    return authCookie;
  }
}
