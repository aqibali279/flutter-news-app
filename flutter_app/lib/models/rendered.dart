class Rendered {
  String textString;

  Rendered(this.textString);

  factory Rendered.fromJson(Map<String, dynamic> json) {
    return Rendered(json['rendered']);
  }
}
