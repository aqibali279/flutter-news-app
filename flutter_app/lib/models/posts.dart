class PostsList {
  List<Post> posts;

  PostsList(this.posts);

  factory PostsList.fromJson(dynamic parsedJson,
      {bool isFromDatabase = false}) {
    var posts = List<Post>();

    try {
      String message = parsedJson['message'];
      return null;
    } catch (x) {
      for (var item in parsedJson) {
        var post = isFromDatabase ? Post.from(item) : Post.fromJson(item);
        posts.add(post);
      }

      return PostsList(posts);
    }
  }
}

class Post {
  int id;
  String title;
  String imageUrl;
  String link;
  String content;

  Post(this.id, this.title, this.imageUrl, this.link, this.content);

  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(
        json['id'],
        json['title']['rendered'],
        json["jetpack_featured_media_url"],
        json["link"],
        json['content']['rendered']);
  }

  factory Post.from(Map<String, dynamic> json) {
    return Post(
        json['id'],
        json['title'],
        json['imageUrl'],
        json['link'],
        json['content']);
  }

  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    if (id != null) {
      map['id'] = id;
    }
    map['title'] = title;
    map['imageUrl'] = imageUrl;
    map['link'] = link;
    map['content'] = content;

    return map;
  }
}
