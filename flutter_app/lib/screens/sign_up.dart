import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_app/utlis/constants.dart';
import 'package:flutter_app/custom_widgets/text_field.dart';
import 'package:flutter_app/network/authentication_service.dart';

class SignUpScreen extends StatefulWidget {
  @override
  SignUpScreenState createState() => SignUpScreenState();
}

class SignUpScreenState extends State<SignUpScreen> {
  var nameNode = FocusNode();
  var emailNode = FocusNode();
  var usernameNode = FocusNode();
  var passwordNode = FocusNode();
  var confirmPasswordNode = FocusNode();

  var nameController = TextEditingController();
  var emailController = TextEditingController();
  var usernameController = TextEditingController();
  var passwordController = TextEditingController();
  var confirmPasswordController = TextEditingController();

  String nameError,
      emailError,
      usernameError,
      passwordError,
      confirmPasswordError;

  var _key = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _key,
        appBar: AppBar(
          title: Text(Constants.SIGN_UP.trim(),
              style: TextStyle(fontWeight: FontWeight.w400)),
        ),
        body: GestureDetector(
          child: ListView(
            padding: EdgeInsets.all(20),
            children: <Widget>[
              Container(
                  margin: EdgeInsets.fromLTRB(0, 20, 0, 20),
                  child: Text(
                    Constants.WELCOME,
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.w300),
                    textAlign: TextAlign.center,
                  )),
              Container(
                  margin: EdgeInsets.fromLTRB(0, 20, 0, 10),
                  child: CustomTextField(
                    context,
                    Constants.NAME_HINT,
                    Constants.NAME,
                    nameError,
                    Icons.account_box,
                    nameController,
                    focusNode: nameNode,
                    nextFocusNode: emailNode,
                    onChanged: (text) {
                      setState(() {
                        nameError = null;
                      });
                    },
                  )),
              Container(
                  margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                  child: CustomTextField(
                    context,
                    Constants.EMAIL_HINT,
                    Constants.EMAIL,
                    emailError,
                    Icons.email,
                    emailController,
                    focusNode: emailNode,
                    nextFocusNode: usernameNode,
                    onChanged: (text) {
                      setState(() {
                        emailError = null;
                      });
                    },
                  )),
              Container(
                  margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                  child: CustomTextField(
                    context,
                    Constants.USERNAME_HINT,
                    Constants.USERNAME,
                    usernameError,
                    Icons.account_circle,
                    usernameController,
                    focusNode: usernameNode,
                    nextFocusNode: passwordNode,
                    onChanged: (text) {
                      setState(() {
                        usernameError = null;
                      });
                    },
                  )),
              Container(
                  margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                  child: CustomTextField(
                    context,
                    Constants.PASSWORD_HINT,
                    Constants.PASSWORD,
                    passwordError,
                    Icons.lock,
                    passwordController,
                    focusNode: passwordNode,
                    nextFocusNode: confirmPasswordNode,
                    onChanged: (text) {
                      setState(() {
                        passwordError = null;
                      });
                    },
                  )),
              Container(
                  margin: EdgeInsets.fromLTRB(0, 10, 0, 20),
                  child: CustomTextField(
                    context,
                    Constants.PASSWORD_HINT,
                    Constants.CONFIRM_PASSWORD,
                    confirmPasswordError,
                    Icons.lock,
                    confirmPasswordController,
                    focusNode: confirmPasswordNode,
                    nextFocusNode: FocusNode(),
                    onChanged: (text) {
                      setState(() {
                        confirmPasswordError = null;
                      });
                    },
                  )),
              Container(
                  margin: EdgeInsets.fromLTRB(0, 20, 0, 20),
                  child: RaisedButton(
                    color: Colors.blue,
                    textColor: Colors.white,
                    child: Text(Constants.SIGN_UP.trim()),
                    onPressed: () async {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          });
                      var flag = await _signUp();
                      Navigator.pop(context);
                      if (flag) {
                        _showSnackBar(Constants.SIGN_UP_SUCCESS);
                        Future.delayed(Duration(seconds: 2), () {
                          Navigator.pop(context);
                        });
                      }
                    },
                  )),
            ],
          ),
          onTap: () {
            FocusScope.of(context).requestFocus(FocusNode());
          },
        ));
  }

  Future<bool> _signUp() async {
    var name = nameController.text;
    var email = emailController.text;
    var username = usernameController.text;
    var password = passwordController.text;
    var confirmPassword = confirmPasswordController.text;

    if (name.isEmpty) {
      setState(() {
        nameError = Constants.NAME_ERROR;
      });
    }

    if (email.isEmpty) {
      setState(() {
        emailError = Constants.EMAIL_ERROR;
      });
    }

    if (username.isEmpty) {
      setState(() {
        usernameError = Constants.USERNAME_ERROR;
      });
    }

    if (password.isEmpty) {
      setState(() {
        passwordError = Constants.PASSWORD_ERROR;
      });
    }

    if (confirmPassword.isEmpty) {
      setState(() {
        confirmPasswordError = Constants.PASSWORD_ERROR;
      });
    }

    if (name.isEmpty ||
        email.isEmpty ||
        username.isEmpty ||
        password.isEmpty ||
        confirmPassword.isEmpty) {
      return false;
    }

    if (password != confirmPassword) {
      _showSnackBar(Constants.PASSWORD_DID_NOT_MATCH);
      return false;
    }

    var authCookie = await AuthenticationService.instance
        .signUp(name, email, username, password);
    if (authCookie.error == null) {
      return true;
    } else {
      _showSnackBar(authCookie.error);
      return false;
    }
  }

  _showSnackBar(String message) {
    final snackBar = SnackBar(
      content: Text(message),
      duration: Duration(seconds: 1),
    );
    _key.currentState.showSnackBar(snackBar);
  }
}
