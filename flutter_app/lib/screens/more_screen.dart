import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_app/network/posts_service.dart';
import 'package:flutter_app/models/posts.dart';
import 'package:flutter_app/models/categories.dart';
import 'package:flutter_app/utlis/constants.dart';
import 'package:flutter_app/cells/home_item.dart';
import 'package:paging/paging.dart';
import "package:pull_to_refresh/pull_to_refresh.dart";

class MoreScreen extends StatefulWidget {
  final Category category;

  MoreScreen(this.category);

  @override
  State<StatefulWidget> createState() {
    return MoreScreenState(category);
  }
}

class MoreScreenState extends State<MoreScreen> {
  Category category;
  var posts = List<Post>();
  var pgNo = 1;
  var isLast = false;
  var _refreshController = RefreshController();
  var _isFirstRequest = true;

  MoreScreenState(this.category);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(category.name,
              style: TextStyle(fontWeight: FontWeight.w400)),
        ),
        body: Pagination<Post>(
            pageBuilder: (size) => _getPosts(),
        itemBuilder: (index,post) => HomeItem(post,category)));

//        FutureBuilder<List<Post>>(
//            future: _getPosts(),
//            builder: (context, snapshot) {
//              if (snapshot.hasData && snapshot.data != null) {
//                var posts = snapshot.data;
//
//                return GridView.count(
//                      controller: ScrollController(),
//                        crossAxisCount: 3,
//                        childAspectRatio: 0.80,
//                        primary: true,
//                        children: List.generate(posts.length, (index) {
//                          return MoreItem(posts[index]);
//                        }));
//              }
//              return Center(
//                child: CircularProgressIndicator(),
//              );
//            }));
  }

  // mocking a network call
  Future<List<Post>> _getPosts() async {

    if (isLast){
      return null;
    }

    var posts = await PostsService.instance.loadPosts(category.id, 5, pgNo);

    if (posts == null) {
      if (!_isFirstRequest)
        _refreshController.sendBack(true, RefreshStatus.failed);
      isLast = true;
    } else if (!_isFirstRequest)
      _refreshController.sendBack(true, RefreshStatus.completed);

    pgNo++;
    return posts;
  }
}
