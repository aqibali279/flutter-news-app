import 'package:flutter/material.dart';
import 'package:flutter_app/utlis/database_helper.dart';
import 'package:flutter_app/utlis/constants.dart';
import 'package:flutter_app/utlis/share.dart';
import 'package:flutter_app/models/posts.dart';

class PopUpMenu extends StatefulWidget {
  final Post _post;
  final Color color;

  PopUpMenu(this._post, this.color);

  @override
  PopUpMenuState createState() => PopUpMenuState(_post, color);
}

class PopUpMenuState extends State<PopUpMenu> {
  String _item;
  Post _post;
  Color _color;

  PopUpMenuState(this._post, this._color);

  @override
  Widget build(BuildContext context) {
    setItem();

    return PopupMenuButton<String>(
      itemBuilder: (context) {
        return [Constants.SHARE, _item]
            .map((String choice) => PopupMenuItem<String>(
                  value: choice,
                  child: Text(choice),
                ))
            .toList();
      },
      icon: Icon(
        Icons.more_vert,
        color: _color,
      ),
      onSelected: (item) {
        switch (item) {
          case Constants.SAVE:
            DatabaseHelper().insertPost(_post);
            setItem();
            break;
          case Constants.REMOVE:
            DatabaseHelper().deletePost(_post);
            setItem();
            break;
          case Constants.SHARE:
            ShareUtil.instance.share(context, _post.id);
            break;
        }
      },
    );
  }

  setItem() async {
    var isSaved = await DatabaseHelper().isPostSaved(_post);
    _item = isSaved ? Constants.REMOVE : Constants.SAVE;
  }
}
