import 'package:flutter/material.dart';

class CustomTextField extends TextField {

  CustomTextField(
      BuildContext context,
      String hint,
      String label,
      String error,
      IconData icon,
      TextEditingController controller,
      {FocusNode focusNode,
      FocusNode nextFocusNode,
      ValueChanged<String> onChanged})
      : super(
            textAlign: TextAlign.left,
            style: TextStyle(fontSize: 12, color: Colors.black87),
            textInputAction: TextInputAction.next,
            focusNode: focusNode,
            onSubmitted: (text) {
              FocusScope.of(context).requestFocus(nextFocusNode);
            },
            onChanged: onChanged,
            controller: controller,
            decoration: InputDecoration(
                suffixIcon: Icon(icon),
                hintText: hint,
                labelText: label,
                errorText: error,
                border: OutlineInputBorder()));
}
